package com.software.client;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.software.client.model.rest.AverageWorkExperience;
import com.software.client.model.rest.Statistic;
import com.software.client.rest.EmployeeClientController;
import com.software.service.RestDaoApplication;
import com.software.service.model.rest.EmployeeResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(classes = RestDaoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class EmployeeClientTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private EmployeeClientController empService;

    @BeforeAll
    static void setup() {
        MockitoAnnotations.initMocks(EmployeeClientTest.class);
    }

    @Test
    void topPostTest_OnlyOnePost() {
        String postName = "postName";
        int employersCount = 1;
        when(restTemplate.exchange(any(),
                                  any(HttpMethod.class),
                                  any(),
                                  ArgumentMatchers.<Class<EmployeeResponse[]>>any(),
                                  ArgumentMatchers.<Object[]>any()))
            .thenReturn(new ResponseEntity<>(generateArray(postName, employersCount), HttpStatus.OK));
        Statistic statistic = empService.getTopPost();
        Assertions.assertEquals(postName, statistic.getPostname());
        Assertions.assertEquals(employersCount, statistic.getEmployersCount());
    }

    @Test
    void topPostTest_OnlyTwoPost() {
        String postName = "postName";
        int employersCount = 1000;
        EmployeeResponse[] both = Stream.concat(
            Arrays.stream(generateArray(postName, employersCount)),
            Arrays.stream(generateArray("anotherPost", 999)))
            .toArray(EmployeeResponse[]::new);

        when(restTemplate.exchange(any(),
                                   any(HttpMethod.class),
                                   any(),
                                   ArgumentMatchers.<Class<EmployeeResponse[]>>any(),
                                   ArgumentMatchers.<Object[]>any()))
            .thenReturn(new ResponseEntity<>(both, HttpStatus.OK));
        Statistic statistic = empService.getTopPost();
        Assertions.assertEquals(postName, statistic.getPostname());
        Assertions.assertEquals(employersCount, statistic.getEmployersCount());
    }

    private EmployeeResponse[] generateArray(String postName, int employersCount){
        EmployeeResponse[] employeeResponses = new EmployeeResponse[employersCount];
        for(int i = 0 ; i < employersCount; i++){
            EmployeeResponse employeeResponse = new EmployeeResponse();
            employeeResponse.setPost(postName);
            employeeResponses[i] = employeeResponse;
        }
        return employeeResponses;
    }

    //TODO:
    @Test
    void averageWorkExperienceTest(){
        String postName = "postName";
        int employersCount = 1;
        when(restTemplate.exchange(any(),
                                   any(HttpMethod.class),
                                   any(),
                                   ArgumentMatchers.<Class<EmployeeResponse[]>>any(),
                                   ArgumentMatchers.<Object[]>any()))
            .thenReturn(new ResponseEntity<>(generateArray(postName, employersCount), HttpStatus.OK));
        AverageWorkExperience averageWorkExperience = empService.getAverageWorkExperience();

    }

    private <T, E> ResponseEntity<E> doRequest(HttpMethod method, String url, T t, Class<E> clazz,
                                               Object... uriVariables) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<T> entity = new HttpEntity<T>(t, headers);
        return restTemplate.exchange(url, method, entity, clazz, uriVariables);
    }
}
