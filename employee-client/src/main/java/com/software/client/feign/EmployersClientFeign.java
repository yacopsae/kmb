package com.software.client.feign;


import com.software.service.model.rest.EmployeeResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("employers-service")
public interface EmployersClientFeign {

    @RequestMapping("/employees")
    Iterable<EmployeeResponse> getAllEmployees();

}
