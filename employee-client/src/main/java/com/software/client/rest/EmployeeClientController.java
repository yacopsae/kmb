package com.software.client.rest;

import com.netflix.discovery.EurekaClient;
import com.software.client.feign.EmployersClientFeign;
import com.software.client.model.rest.AverageWorkExperience;
import com.software.client.model.rest.Statistic;
import com.software.service.model.rest.EmployeeResponse;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class EmployeeClientController {

    private final EmployersClientFeign employersClientFeign;

    @GetMapping("/post/top")
    @ResponseStatus(code = HttpStatus.OK)
    public Statistic getTopPost() {
        Iterable<EmployeeResponse> allEmployees = employersClientFeign.getAllEmployees();
        if (Objects.nonNull(allEmployees) && allEmployees.iterator().hasNext()) {
            Map<String, Long> counter = new HashMap<>();
            StreamSupport.stream(allEmployees.spliterator(), false)
                .forEach(entry -> {
                    String postName = entry.getPost();
                    long count = 1L;
                    if (counter.containsKey(postName))
                        count = counter.get(postName) + 1;
                    counter.put(postName, count);
                });
            Optional<Entry<String, Long>> max = counter.entrySet().stream().max((Entry.comparingByValue()));
            if (max.isPresent()) {
                Entry<String, Long> result = max.get();
                return new Statistic(result.getKey(), result.getValue());
            }
        }
        return null;
    }


    @GetMapping("/averageWorkExperience")
    @ResponseStatus(code = HttpStatus.OK)
    public AverageWorkExperience getAverageWorkExperience() {
        Iterable<EmployeeResponse> allEmployees = employersClientFeign.getAllEmployees();
        if(Objects.nonNull(allEmployees) && allEmployees.iterator().hasNext()){
            OptionalDouble average = StreamSupport.stream(allEmployees.spliterator(), false)
                .map(this::getWorkExperience)
                .mapToLong(Duration::toMillis)
                .average();
            if (average.isPresent())
                return calculateAverageExp((long)average.getAsDouble());
        }
        return new AverageWorkExperience(0,0,0);
    }

    private AverageWorkExperience calculateAverageExp(Long millis){
        Duration duration = Duration.ofMillis(millis);
        long seconds = duration.get(ChronoUnit.SECONDS);
        long dy = TimeUnit.SECONDS.toDays(seconds);
        final long yr = dy / 365;
        dy %= 365;
        final long mn = dy / 30;
        dy %= 30;
        return new AverageWorkExperience(yr, mn, dy);
    }

    private Duration getWorkExperience(EmployeeResponse employeeResponse) {
        if (Objects.isNull(employeeResponse.getTerminationDate()))
            return Duration.between(employeeResponse.getRecruitmentDate(), Instant.now());
        else
            return Duration.between(employeeResponse.getRecruitmentDate(), employeeResponse.getTerminationDate());
    }
}
