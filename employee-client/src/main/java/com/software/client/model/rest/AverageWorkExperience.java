package com.software.client.model.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AverageWorkExperience {
    private long years;
    private long month;
    private long days;
}
