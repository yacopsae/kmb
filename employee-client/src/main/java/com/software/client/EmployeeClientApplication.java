package com.software.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class EmployeeClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmployeeClientApplication.class, args);
    }
}
