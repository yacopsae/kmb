/**
    @author Pavel Karev
 */

create schema TEST;

create table TEST.employees (
    id decimal(19,2) not null identity,
    surname varchar(255) not null,
    name varchar(255) not null,
    second_name varchar(255) null,
    post varchar(255) not null,
    recruitment_date date not null,
    termination_date date
);

INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_1', 'second_name_1', 'post_1', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_2', 'second_name_2', 'post_2', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_3', 'second_name_3', 'post_3', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_4', 'second_name_4', 'post_4', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_5', 'second_name_5', 'post_5', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_6', 'second_name_6', 'post_6', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_7', 'second_name_7', 'post_7', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_8', 'second_name_8', 'post_8', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_9', 'second_name_9', 'post_9', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_10', 'second_name_10', 'post_10', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_11', 'second_name_11', 'post_11', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_12', 'second_name_12', 'post_12', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_13', 'second_name_13', 'post_13', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_14', 'second_name_14', 'post_14', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_15', 'second_name_15', 'post_15', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_16', 'second_name_16', 'post_16', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_17', 'second_name_17', 'post_17', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_18', 'second_name_18', 'post_18', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_19', 'second_name_19', 'post_19', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_20', 'second_name_20', 'post_20', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_21', 'second_name_21', 'post_21', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_22', 'second_name_22', 'post_22', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_23', 'second_name_23', 'post_23', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_24', 'second_name_24', 'post_24', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_25', 'second_name_25', 'post_25', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_26', 'second_name_26', 'post_26', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_27', 'second_name_27', 'post_27', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_28', 'second_name_28', 'post_28', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_29', 'second_name_29', 'post_29', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_30', 'second_name_30', 'post_30', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_31', 'second_name_31', 'post_31', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_32', 'second_name_32', 'post_32', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_33', 'second_name_33', 'post_33', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_34', 'second_name_34', 'post_34', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_35', 'second_name_35', 'post_35', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_36', 'second_name_36', 'post_36', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_37', 'second_name_37', 'post_37', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_38', 'second_name_38', 'post_38', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_39', 'second_name_39', 'post_39', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_40', 'second_name_40', 'post_40', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_41', 'second_name_41', 'post_41', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_42', 'second_name_42', 'post_42', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_43', 'second_name_43', 'post_43', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_44', 'second_name_44', 'post_44', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_45', 'second_name_45', 'post_45', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_46', 'second_name_46', 'post_46', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_47', 'second_name_47', 'post_47', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_48', 'second_name_48', 'post_48', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_49', 'second_name_49', 'post_49', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_50', 'second_name_50', 'post_50', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_51', 'second_name_51', 'post_51', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_52', 'second_name_52', 'post_52', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_53', 'second_name_53', 'post_53', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_54', 'second_name_54', 'post_54', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_55', 'second_name_55', 'post_55', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_56', 'second_name_56', 'post_56', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_57', 'second_name_57', 'post_57', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_58', 'second_name_58', 'post_58', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_59', 'second_name_59', 'post_59', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_60', 'second_name_60', 'post_60', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_61', 'second_name_61', 'post_61', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_62', 'second_name_62', 'post_62', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_63', 'second_name_63', 'post_63', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_64', 'second_name_64', 'post_64', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_65', 'second_name_65', 'post_65', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_66', 'second_name_66', 'post_66', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_67', 'second_name_67', 'post_67', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_68', 'second_name_68', 'post_68', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_69', 'second_name_69', 'post_69', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_70', 'second_name_70', 'post_70', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_71', 'second_name_71', 'post_71', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_72', 'second_name_72', 'post_72', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_73', 'second_name_73', 'post_73', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_74', 'second_name_74', 'post_74', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_75', 'second_name_75', 'post_75', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_76', 'second_name_76', 'post_76', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_77', 'second_name_77', 'post_77', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_78', 'second_name_78', 'post_78', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_79', 'second_name_79', 'post_79', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_80', 'second_name_80', 'post_80', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_81', 'second_name_81', 'post_81', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_82', 'second_name_82', 'post_82', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_83', 'second_name_83', 'post_83', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_84', 'second_name_84', 'post_84', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_85', 'second_name_85', 'post_85', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_86', 'second_name_86', 'post_86', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_87', 'second_name_87', 'post_87', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_88', 'second_name_88', 'post_88', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_89', 'second_name_89', 'post_89', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_90', 'second_name_90', 'post_90', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_91', 'second_name_91', 'post_91', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_92', 'second_name_92', 'post_92', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_93', 'second_name_93', 'post_93', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_94', 'second_name_94', 'post_94', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_95', 'second_name_95', 'post_95', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_96', 'second_name_96', 'post_96', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_97', 'second_name_97', 'post_97', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_98', 'second_name_98', 'post_98', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
INSERT INTO TEST.employees (surname, name, second_name, post, recruitment_date) VALUES ('surname_!', 'name_99', 'second_name_99', 'post_99', to_date('2017-04-21T00:00:00Z', 'YYYY-MM-DDTHH24:MI:SS"Z"'));
