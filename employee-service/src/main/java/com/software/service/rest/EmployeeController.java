package com.software.service.rest;

import com.netflix.discovery.EurekaClient;
import com.software.service.mapper.EmployeeMapper;
import com.software.service.model.rest.EmployeeResponse;
import com.software.service.model.rest.NewEmployeeRequest;
import com.software.service.model.rest.UpdateEmployeeRequest;
import com.software.service.repository.EmployeeRepository;
import java.math.BigInteger;
import javax.sql.DataSource;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@lombok.Value
@RestController
public class EmployeeController {

    String appName;
    EurekaClient eurekaClient;
    EmployeeRepository employeeRepository;
    EmployeeMapper employeeMapper;
    DataSource dataSource;

    @Autowired
    public EmployeeController(@Lazy EurekaClient eurekaClient,
                              @Value("${spring.application.name}") String appName,
                              EmployeeRepository employeeRepository,
                              EmployeeMapper employeeMapper,
                              DataSource dataSource) {
        this.eurekaClient = eurekaClient;
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
        this.appName = appName;
        this.dataSource = dataSource;
    }

    @PostMapping("/employee")
    @ResponseStatus(code = HttpStatus.CREATED)
    public EmployeeResponse createEmployee(@Valid @RequestBody NewEmployeeRequest employee) {
        return employeeMapper
            .employeeToEmployeeResponse(employeeRepository.save(employeeMapper.newEmployeeToEmployee(employee)));
    }

    @PutMapping("/employee")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public EmployeeResponse updateEmployee(@Valid @RequestBody UpdateEmployeeRequest employee) {
        return employeeMapper
            .employeeToEmployeeResponse(employeeRepository.save(employeeMapper.updateEmployeeToEmployee(employee)));
    }

    @DeleteMapping("/employee/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteEmployee(@PathVariable Long id) {
        employeeRepository.deleteById(BigInteger.valueOf(id));
    }

    @GetMapping("/employees")
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<EmployeeResponse> getAllEmployees() {
        return employeeMapper.employeeToEmployeeResponse(employeeRepository.findAll());
    }
}
