package com.software.service.mapper;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import org.springframework.stereotype.Service;

@Service
public class JavaTimeMapper {

    public LocalDate asLocalDate(Instant instant) {
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, zoneId);
        return zdt.toLocalDate();
    }

    public Instant asInstant(LocalDate localDate) {
        return  localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }
}
