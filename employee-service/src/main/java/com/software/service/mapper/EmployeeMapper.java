package com.software.service.mapper;

import com.software.service.model.db.Employees;
import com.software.service.model.rest.NewEmployeeRequest;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import com.software.service.model.rest.EmployeeResponse;
import com.software.service.model.rest.UpdateEmployeeRequest;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", uses = JavaTimeMapper.class)
public interface EmployeeMapper {

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "secondName", source = "secondName")
    @Mapping(target = "post", source = "post")
    @Mapping(target = "recruitmentDate", source = "recruitmentDate")
    @Mapping(target = "terminationDate", source = "terminationDate", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    Employees newEmployeeToEmployee(NewEmployeeRequest employeeRequest);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "secondName", source = "secondName")
    @Mapping(target = "post", source = "post")
    @Mapping(target = "recruitmentDate", source = "recruitmentDate")
    @Mapping(target = "terminationDate", source = "terminationDate", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    Employees updateEmployeeToEmployee(UpdateEmployeeRequest employeeRequest);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "secondName", source = "secondName")
    @Mapping(target = "post", source = "post")
    @Mapping(target = "recruitmentDate", source = "recruitmentDate")
    @Mapping(target = "terminationDate", source = "terminationDate", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    EmployeeResponse employeeToEmployeeResponse(Employees employees);

    @IterableMapping(qualifiedByName = "employeeToEmployeeResponse")
    Iterable<EmployeeResponse> employeeToEmployeeResponse(Iterable<Employees> employees);
}
