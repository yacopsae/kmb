package com.software.service.model.db;

import java.math.BigInteger;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "TEST")
public class Employees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, name = "ID")
    private BigInteger id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "SECOND_NAME")
    private String secondName;

    @Column(name = "POST")
    private String post;

    @Column(name = "RECRUITMENT_DATE")
    private LocalDate recruitmentDate;

    @Column(name = "TERMINATION_DATE")
    private LocalDate terminationDate;
}
