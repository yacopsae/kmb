package com.software.service.model.rest;

import java.math.BigInteger;
import java.time.Instant;
import lombok.Data;

@Data
public class EmployeeResponse {
    private BigInteger id;
    private String name;
    private String surname;
    private String secondName;
    private String post;
    private Instant recruitmentDate;
    private Instant terminationDate;
}
