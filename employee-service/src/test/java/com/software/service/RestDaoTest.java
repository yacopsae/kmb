package com.software.service;

import com.software.service.model.db.Employees;
import com.software.service.model.rest.NewEmployeeRequest;
import com.software.service.model.rest.UpdateEmployeeRequest;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.RestTemplate;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

@SpringBootTest(classes = RestDaoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RestDaoTest {

    private final JdbcTemplate jdbcTemplate;
    private final PodamFactory podamFactory;
    private final RestTemplate restTemplate;
    private final Employees DUMMY;
    private final String INSERT_EMPLOYEES;

    @Autowired
    RestDaoTest(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        PodamFactory podamFactory = new PodamFactoryImpl();
        podamFactory.getStrategy().addOrReplaceTypeManufacturer(BigInteger.class,
                                                                (dataProviderStrategy, attributeMetadata, map) -> BigInteger
                                                                    .valueOf(new Random().nextLong()));
        this.podamFactory = podamFactory;
        this.restTemplate = new RestTemplate(getClientHttpRequestFactory());
        this.DUMMY = podamFactory.manufacturePojo(Employees.class);
        this.DUMMY.setId(BigInteger.valueOf(1L));
        this.INSERT_EMPLOYEES = String
            .format("INSERT INTO employees (surname, name, second_name, post, recruitment_date, termination_date) "
                        + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s');",
                    DUMMY.getSurname(), DUMMY.getName(), DUMMY.getSecondName(), DUMMY.getPost(),
                    DUMMY.getRecruitmentDate(),
                    DUMMY.getTerminationDate());
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 5000;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout);
        return clientHttpRequestFactory;
    }

    @Test
    @DirtiesContext
    void createEmployee() {
        String url = "http://localhost:8080/employee";
        NewEmployeeRequest source = podamFactory.manufacturePojo(NewEmployeeRequest.class);
        ResponseEntity<Employees> employeesResponseEntity = restTemplate.postForEntity(url, source, Employees.class);
        Assertions.assertEquals(HttpStatus.CREATED, employeesResponseEntity.getStatusCode());
        Employees target = employeesResponseEntity.getBody();
        assertEmployees(source, target);
        String SELECT_FROM_EMPLOYEES = "SELECT id, name, surname, second_name, post, recruitment_date, termination_date FROM \"employees\"";
        Employees targetDb = jdbcTemplate.query(SELECT_FROM_EMPLOYEES, new EmployeesExtractor<>(Employees.class));
        assertEmployees(source, targetDb);
        Assertions.assertEquals(BigInteger.valueOf(1L), targetDb.getId());
    }

    @Test
    @DirtiesContext
    void updateEmployee() {
        String url = "http://localhost:8080/employee";
        jdbcTemplate.execute(INSERT_EMPLOYEES);
        UpdateEmployeeRequest source = podamFactory.manufacturePojo(UpdateEmployeeRequest.class);
        source.setId(1L);
        ResponseEntity<Employees> employeesResponseEntity = doRequest(HttpMethod.PUT, url, source, Employees.class);
        Assertions.assertEquals(HttpStatus.ACCEPTED, employeesResponseEntity.getStatusCode());
        Employees target = employeesResponseEntity.getBody();
        assertEmployees(source, target);
    }

    @Test
    @DirtiesContext
    void deleteEmployee() {
        String url = "http://localhost:8080/employee/{id}";
        jdbcTemplate.update("TRUNCATE TABLE employees RESTART IDENTITY;");
        jdbcTemplate.execute(INSERT_EMPLOYEES);
        ResponseEntity<String> responseEntity = doRequest(HttpMethod.DELETE, url, null, String.class, 1L);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Long count = jdbcTemplate.query("SELECT COUNT(1) FROM employees;", (resultSet) -> {
            if (resultSet.next())
                return resultSet.getLong(1);
            return 0L;
        });
        Assertions.assertEquals(0L, count);
    }

    @Test
    @DirtiesContext
    void getAllEmployees() {
        String url = "http://localhost:8080/employees";
        jdbcTemplate.execute(INSERT_EMPLOYEES);
        ResponseEntity<Employees[]> responseEntity = doRequest(HttpMethod.GET, url, null, Employees[].class);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Employees[] employees = responseEntity.getBody();
        Assertions.assertNotNull(employees);
        for(Employees employee : employees){
            assertEmployees(DUMMY, employee);
        }
    }


    private static void assertEmployees(NewEmployeeRequest source, Employees target) {
        Assertions.assertNotNull(target);
        Assertions.assertEquals(source.getName(), target.getName());
        Assertions.assertEquals(source.getSurname(), target.getSurname());
        Assertions.assertEquals(source.getSecondName(), target.getSecondName());
        Assertions.assertEquals(source.getPost(), target.getPost());
        Assertions.assertEquals(instantToLocalDate(source.getRecruitmentDate()), target.getRecruitmentDate());
        Assertions.assertEquals(instantToLocalDate(source.getTerminationDate()), target.getTerminationDate());
    }

    private static void assertEmployees(UpdateEmployeeRequest source, Employees target) {
        Assertions.assertNotNull(target);
        Assertions.assertEquals(source.getName(), target.getName());
        Assertions.assertEquals(source.getSurname(), target.getSurname());
        Assertions.assertEquals(source.getSecondName(), target.getSecondName());
        Assertions.assertEquals(source.getPost(), target.getPost());
        Assertions.assertEquals(instantToLocalDate(source.getRecruitmentDate()), target.getRecruitmentDate());
        Assertions.assertEquals(instantToLocalDate(source.getTerminationDate()), target.getTerminationDate());
        Assertions.assertEquals(source.getId(), target.getId().longValue());
    }

    private static void assertEmployees(Employees source, Employees target) {
        Assertions.assertNotNull(target);
        Assertions.assertEquals(source.getName(), target.getName());
        Assertions.assertEquals(source.getSurname(), target.getSurname());
        Assertions.assertEquals(source.getSecondName(), target.getSecondName());
        Assertions.assertEquals(source.getPost(), target.getPost());
        Assertions.assertEquals(source.getRecruitmentDate(), target.getRecruitmentDate());
        Assertions.assertEquals(source.getTerminationDate(), target.getTerminationDate());
        Assertions.assertEquals(source.getId(), target.getId());
    }


    private static LocalDate instantToLocalDate(Instant instant) {
        ZoneId zoneId = ZoneId.of("Europe/Moscow");
        ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, zoneId);
        return zdt.toLocalDate();
    }

    private <T, E> ResponseEntity<E> doRequest(HttpMethod method, String url, T t, Class<E> clazz,
                                               Object... uriVariables) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<T> entity = new HttpEntity<T>(t, headers);
        return restTemplate.exchange(url, method, entity, clazz, uriVariables);
    }

}
