package com.software.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.software.service.model.db.Employees;

public class EmployeesExtractor<T> implements ResultSetExtractor<T> {

    private final Class<T> clazz;

    public EmployeesExtractor(Class<T> clazz) {
        this.clazz = clazz;
    }

    private Employees map(ResultSet resultSet) throws SQLException {
        return new Employees(
            resultSet.getBigDecimal(1).toBigInteger(),
            resultSet.getString(2),
            resultSet.getString(3),
            resultSet.getString(4),
            resultSet.getString(5),
            resultSet.getDate(6).toLocalDate(),
            resultSet.getDate(7).toLocalDate()
        );
    }

    @Override
    public T extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        if (clazz.isAssignableFrom(Iterable.class)) {
            List<Employees> employees = new ArrayList<>();
            while (resultSet.next()) {
                employees.add(map(resultSet));
            }
            return (T) employees;
        } else if (resultSet.next())
            return (T) map(resultSet);

        return null;
    }
}